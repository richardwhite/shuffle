# USAGE #

Simply run index.php from the command line!

Code is fairly well documented, proof that the deck has been shuffled according to the requirement can be read in the code comments.

# BACKGROUND #

This task is to create simple class files to showcase your PHP programming skills. Where you would normally want to produce the least amount of code to achieve a given task, this is a special case where we are looking to see the range of your abilities. Therefore, please feel free to implement your solution in an elaborate fashion, although we would expect you to take no more than 2 hours to complete the task. Your implementation and the reasoning behind it will then form the basis of discussion with one of our engineers later in the hiring process.

The scenario is as follows:
You have a deck of 52 cards, comprised of 4 suits (hearts, clubs, spades and diamonds) each with 13 values (Ace, two, three, four, five, six, seven, eight, nine, ten, jack, queen and king).
There are four players waiting to play around a table.
The deck arrives in perfect sequence (so, ace of hearts is at the bottom, two of hearts is next, etc. all the way up to king of diamonds on the top).

The task is a simple one. Please create a simple command line program that when executed recreates the scenario above and then performs the following two actions:
Shuffle the cards  - We would like to take the deck that is in sequence and shuffle it so that no two cards are still in sequence.
Deal the cards - We would then like to deal seven cards to each player (one card to the each player, then a second card to each player, and so on)

There is no need to necessarily do this in a visual way (for example, simply proving with a test that your deck is shuffled and that the players do now have seven cards will be sufficient)

Please supply your solution as a zip file containing any classes, and documentation, etc that you have produced.