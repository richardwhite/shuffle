<?php

class Util
{
    private $original_deck;

    public function CreateDeck()
    {
        $deck = array();
        $reflector = new ReflectionClass('PlayingCard');
        $card_values = $reflector->getConstants();
        $card_suits = PlayingCard::GetSuits();

        foreach( $card_suits as $card_suit )
        {
            foreach( $card_values as $card_value )
            {
                $deck[] = new $card_suit( $card_value );
            }
        }

        $this->original_deck = $deck;
        return $this;
    }

    /**
     * Checks to ensure that no two cards in a shuffled set are adjacent to their original neighbours
     *
     * @param $deck
     * @return bool
     */
    public function ValidateDeck( $deck )
    {
        $original_deck = $this->original_deck;
        $total_cards = count( $deck );
        $success = true;

        /**
         * Iterate through the shuffled array, ensuring that no two cards
         * in the original deck are still adjacent
         */
        for( $x=0; $x<$total_cards-1; $x++ )
        {
            $prev_element = isset($deck[$x-1]) ? $deck[$x-1] : null;
            $next_element = isset($deck[$x-1]) ? $deck[$x-1] : null;

            /**
             * Check to see whether the previous element matches the original deck order
             */
            if( $prev_element )
            {
                $prev_element_original = isset($original_deck[$x-1]) ? $original_deck[$x-1] : null;

                if( $prev_element === $prev_element_original )
                {
                    $success = false;
                }
            }

            /**
             * Check to see whether the next element matches the original deck order
             */
            if( $next_element )
            {
                $next_element_original = isset($original_deck[$x+1]) ? $original_deck[$x+1] : null;

                if( $next_element === $next_element_original )
                {
                    $success = false;
                }
            }
        }

        return $success;
    }

    /**
     * Returns a shuffled deck of cards where no two cards in the shuffled deck are
     * adjacent to their original neighbours
     *
     * @return mixed
     */
    public function ShuffleDeck()
    {
        $deck = $this->original_deck;
        $total_cards = count( $deck );

        do
        {
            /**
             * Shuffle the deck according to the Fisher–Yates shuffle algorithm
             */
            for( $x=0; $x<$total_cards-1; $x++ )
            {
                $rand_element = rand( $x, $total_cards-1 );
                $rand_card = $deck[$rand_element];
                $deck[$rand_element] = $deck[$x];
                $deck[$x] = $rand_card;
            }
            $success = $this->ValidateDeck( $deck );
        } while(!$success);

        return $deck;
    }

    public function DealCards( $deck, $players )
    {
        $max_rounds = 7;

        do
        {
            /**
             * @type Player $player
             */
            foreach( $players as $player )
            {
                $player->AddCard( array_pop( $deck ) );
            }
            $max_rounds--;
        } while ($max_rounds > 0);

        return $players;
    }
}