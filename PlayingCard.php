<?php

class PlayingCard
{
    CONST CARD_VALUE_ACE   = 1;
    CONST CARD_VALUE_TWO   = 2;
    CONST CARD_VALUE_THREE = 3;
    CONST CARD_VALUE_FOUR  = 4;
    CONST CARD_VALUE_FIVE  = 5;
    CONST CARD_VALUE_SIX   = 6;
    CONST CARD_VALUE_SEVEN = 7;
    CONST CARD_VALUE_EIGHT = 8;
    CONST CARD_VALUE_NINE  = 9;
    CONST CARD_VALUE_TEN   = 10;
    CONST CARD_VALUE_JACK  = 11;
    CONST CARD_VALUE_QUEEN = 12;
    CONST CARD_VALUE_KING  = 13;

    protected $card_suit;
    protected $card_value;

    public function __construct( $card_suit, $card_value )
    {
        $this->card_suit = $card_suit;
        $this->card_value = $card_value;
    }

    public function GetSuit()
    {
        return $this->card_suit;
    }

    public function GetValue()
    {
        return $this->card_value;
    }

    public static function GetSuits()
    {
        return array(
            'Hearts',
            'Clubs',
            'Spades',
            'Diamonds'
        );
    }
}