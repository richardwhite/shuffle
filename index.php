<?php

require('PlayingCard.php');
require('Util.php');

set_include_path('Suit');

function __autoload($class_name) {
    include $class_name . '.php';
}

/**
 * This could be taken as a command line argument if required
 */
$num_players = 4;

$util = new Util();

/**
 * Return a shuffled deck of cards
 */
$deck = $util->CreateDeck()->ShuffleDeck();

/**
 * Create our players
 */
for( $x=0; $x<$num_players; $x++ )
{
    $players[] = new Player( $x );
}

/**
 * Deal cards according to the number of players
 */
$players = $util->DealCards( $deck, $players );

for( $x=0; $x<$num_players; $x++ )
{
    $player_number = $x+1;
    echo "\r\n";
    echo "Player #$player_number's hand";
    echo "\r\n";
    $players[$x]->ShowHand();
}

exit;