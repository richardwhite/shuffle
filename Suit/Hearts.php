<?php

class Hearts extends PlayingCard
{
    public function __construct( $card_value )
    {
        parent::__construct( 'Hearts', $card_value );
    }
}