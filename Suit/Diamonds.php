<?php

class Diamonds extends PlayingCard
{
    public function __construct( $card_value )
    {
        parent::__construct( 'Diamonds', $card_value );
    }
}