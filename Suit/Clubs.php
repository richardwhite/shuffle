<?php

class Clubs extends PlayingCard
{
    public function __construct( $card_value )
    {
        parent::__construct( 'Clubs', $card_value );
    }
}