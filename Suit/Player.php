<?php

class Player
{
    private $hand = array();
    private $id = 0;

    public function __construct( $id )
    {
        $this->id = $id;
    }

    public function AddCard( $card )
    {
        $this->hand[] = $card;
    }

    public function GetID()
    {
        return $this->id;
    }

    public function ShowHand()
    {
        foreach( $this->hand as $card )
        {
            echo $card->GetValue();
            echo ' of '.$card->GetSuit();
            echo "\r\n";
        }
    }
}