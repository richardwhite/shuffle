<?php

class Spades extends PlayingCard
{
    public function __construct( $card_value )
    {
        parent::__construct( 'Spades', $card_value );
    }
}